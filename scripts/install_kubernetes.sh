#!/usr/bin/env bash
export KUBECONFIG=$(pwd)/config/kube_config_rancher-cluster.yml
export RKE_DEPLOY_TEMPLATE=$(pwd)/config/rancher-cluster.yml

# ETAPA 1: Faz o deploy do kubernetes
/usr/local/bin/rke up --config $RKE_DEPLOY_TEMPLATE
COUNTER=0;
TOTAL=3;
while [  $COUNTER -lt $TOTAL ]; do 
    COUNTER=`kubectl get nodes | tr " " "\n" | grep -c "Ready"`;
    echo "Waiting kubernetes nodes (${COUNTER} / ${TOTAL})...";
done

# ETAPA 2: instalação do cert-manager
# Install the CustomResourceDefinition resources separately
/usr/local/bin/kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.14/deploy/manifests/00-crds.yaml

# Create the namespace for cert-manager
/usr/local/bin/kubectl create namespace cert-manager

# Add the Jetstack Helm repository
/usr/local/bin/helm repo add jetstack https://charts.jetstack.io

# Update your local Helm chart repository cache
/usr/local/bin/helm repo update

# Install the cert-manager Helm chart
/usr/local/bin/helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v0.14.0

# verifica a instalação
COUNTER=0;
while [  $COUNTER -lt $TOTAL ]; do 
    COUNTER=`/usr/local/bin/kubectl get pods --namespace cert-manager | tr " " "\n" | grep -c "Running"`;
    echo "Waiting cert-manager nodes (${COUNTER} / ${TOTAL})...";
done


# Rancher Install
# Add the Helm Chart Repository
/usr/local/bin/helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
# Update your local Helm chart repository cache
/usr/local/bin/helm repo update
# Create a Namespace for Rancher
/usr/local/bin/kubectl create namespace cattle-system
# Install Rancher Helm
/usr/local/bin/helm install rancher rancher-stable/rancher --namespace cattle-system --set ingress.tls.source=letsEncrypt --set letsEncrypt.email=$EMAIL --set hostname=$DNS_HOSTNAME
# Check installation status
/usr/local/bin/kubectl -n cattle-system rollout status deploy/rancher