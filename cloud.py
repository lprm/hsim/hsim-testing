import settings
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.deployment import MultiStepDeployment
from libcloud.compute.deployment import ScriptDeployment, SSHKeyDeployment
import requests
from requests.auth import HTTPBasicAuth


class CloudManager():

    def __init__(self, provider, auth):
        cls = get_driver(provider)
        provider_settings = dict((k.lower(), v) for k, v in auth.items())
        self.driver = cls(**provider_settings)

        self._sizes = self.driver.list_sizes()
        self._locations = self.driver.list_locations()
        self._images = self.driver.list_images()

    def init_key(self, name, pub_key_file):
        try:
            self.key_pair = self.driver.create_key_pair(
                name=name, public_key=open(pub_key_file, 'r').read())
        except:
            self.key_pair = self.driver.get_key_pair(name)

    def connect(self, username, password):
        pass

    def configure(self):
        pass

    def get_deployment_scripts(self, scripts):
        deployment_scripts = []
        for script in scripts:
            deployment_scripts.append(ScriptDeployment(script))
        msd = MultiStepDeployment(deployment_scripts)
        return msd

    def _get_size(self, id):
        return next(obj for obj in self._sizes if obj.id == id)

    def _get_image(self, id):
        return next(obj for obj in self._images if obj.name == id)

    def _get_location(self, id):
        return next(obj for obj in self._locations if obj.id == id)

    def update_dns(self, username, password, host, ip):
        payload = {"hostname": host, "myip": ip}
        response = requests.get('http://dynupdate.no-ip.com/nic/update',
                                params=payload, auth=HTTPBasicAuth(username, password))
        return response.status_code == 200

    def create_balancer(self, name, nodes):
        payload = {"name": name,
                   "region": settings.CLOUD_PROVIDER["LOCATION"],
                   "forwarding_rules": [
                       {"entry_protocol": "http", "entry_port": 80, "target_protocol": "http",
                        "target_port": 80, "certificate_id": "", "tls_passthrough": False},
                       {"entry_protocol": "https", "entry_port": 443,
                        "target_protocol": "https", "target_port": 443, "tls_passthrough": True}
                   ],
                   "health_check": {"protocol": "http", "port": 80, "path": "/healthz", "check_interval_seconds": 10, "response_timeout_seconds": 5, "healthy_threshold": 5, "unhealthy_threshold": 3},
                   "sticky_sessions": {"type": "none"},
                   "droplet_ids": nodes
                   }
        headers = {"Authorization": "Bearer %s" %
                   (settings.CLOUD_PROVIDER["AUTH"]["KEY"])}
        response = requests.post("https://api.digitalocean.com/v2/load_balancers", json=payload, headers=headers)
        return response.json()

    def get_balancer(self, id):
        headers = {"Authorization": "Bearer %s" % (settings.CLOUD_PROVIDER["AUTH"]["KEY"])}
        response = requests.get("https://api.digitalocean.com/v2/load_balancers/%s" % (id, ), headers=headers)
        return response.json()

    def create_node(self, name, size, image, location, priv_key_file, deployment_scripts, extra_options):
        deployment = self.get_deployment_scripts(deployment_scripts)
        _size = self._get_size(size)
        _image = self._get_image(image)
        _location = self._get_location(location)
        _extra_options = extra_options.copy()
        _extra_options["ssh_keys"] = [self.key_pair.fingerprint]
        node = self.driver.deploy_node(name=name, size=_size, image=_image, location=_location,
                                       ssh_key=priv_key_file, ex_create_attr=_extra_options, deploy=deployment)
        return node
