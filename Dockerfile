FROM python:3.8.1-alpine3.11

ADD requirements.txt /tmp

RUN apk --update add curl grep libzmq bash \
    && apk add --virtual build-dependencies libffi-dev openssl-dev python-dev build-base \
    && pip install -r /tmp/requirements.txt \
    && apk del build-dependencies

RUN cd /tmp \
    && curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    # RKE install
    && curl -LO https://github.com/rancher/rke/releases/download/`curl -s https://api.github.com/repos/rancher/rke/releases/latest | grep -oP '"tag_name": "\K(.*)(?=")'`/rke_linux-amd64 \
    && chmod +x ./rke_linux-amd64 \
    && mv ./rke_linux-amd64 /usr/local/bin/rke \
    # Helm install
    && curl -LO https://get.helm.sh/helm-`curl -s https://api.github.com/repos/helm/helm/releases/latest | grep -oP '"tag_name": "\K(.*)(?=")'`-linux-amd64.tar.gz \
    && tar -zxvf helm-*.tar.gz \
    && mv linux-amd64/helm /usr/local/bin/helm \
    && rm -rf linux-amd64 helm-*.tar.gz

