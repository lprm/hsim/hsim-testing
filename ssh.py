import paramiko
import os

class SSHClient():
    _client = None
    _priv_key_file = None
    _pub_key_file = None
    _priv_key = None
    _pub_key = None

    def __init__(self, _priv_key_file, _pub_key_file, _key_name):
        self._pub_key_file = _pub_key_file
        self._priv_key_file = _priv_key_file
        self._key_name = _key_name
        self.init_ssh_keys()

    def resolve_host(host, retry=10):
        counter = 0
        response = False
        while (not response and counter < retry):
            try:
                response = socket.gethostbyname(host)
            except socket.gaierror:
                counter += 1
        return response

    def init_ssh_keys(self):
        # check if ssh keys exists
        if not os.path.exists(self._pub_key_file):
            priv_file = open(self._priv_key_file, 'w+')
            pub_file = open(self._pub_key_file, 'w+')
            key = paramiko.RSAKey.generate(2048)
            key.write_private_key(priv_file)
            pub_file.write(self.format_pub_key(self._key_name, key.get_base64()))
            priv_file.close()
            pub_file.close()
        os.chmod(self._priv_key_file, 0o600)
        os.chmod(self._pub_key_file, 0o600)

        self._priv_key = paramiko.rsakey.RSAKey(filename=self._priv_key_file)
        self._pub_key = open(self._pub_key_file, 'r').read()

    def format_pub_key(self, name, key):
        return "%s %s %s" % ("ssh-rsa", key, name)

    def connect(self, host, username):
        self._client = paramiko.SSHClient()
        self._client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self._client.load_system_host_keys()
        self._client.connect(hostname=host, username=username, pkey=self._priv_key)

    def exec_command(self, command):
        stdin, stdout, stderr = self._client.exec_command(command)
        return stdout.read()

    def disconnect(self):
        self._client.close()
