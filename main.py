import ssh
import settings
from templates import loader
from cloud import CloudManager
import subprocess
import requests
from requests.auth import HTTPBasicAuth
from ssh import SSHClient

print("Initializing ssh...")
client = SSHClient(settings.SSH_PRIV_KEY, settings.SSH_PUB_KEY, settings.SSH_KEY_NAME)
client.init_ssh_keys()

print("Initializing provider...")
cloud = CloudManager(
    settings.CLOUD_PROVIDER["PROVIDER"], auth=settings.CLOUD_PROVIDER["AUTH"])
cloud.init_key(settings.SSH_KEY_NAME, settings.SSH_PUB_KEY)

# cria os nós do rancher
print("Creating nodes...")
nodes = []
for i in range(1, 4):
    node = cloud.create_node("rancher-%s" % (i, ), settings.CLOUD_PROVIDER["SIZE"],
                             settings.CLOUD_PROVIDER["IMAGE"], settings.CLOUD_PROVIDER["LOCATION"],
                             settings.SSH_PRIV_KEY,
                             [settings.DOCKER_INSTALL_SCRIPT], settings.CLOUD_PROVIDER["EXTRA_OPTIONS"])
    nodes.append(node)

# criate load balancer
print("Initializing balancer...")
balancer = cloud.create_balancer("testing-lb-01", [int(node.id) for node in nodes])
while (not balancer["load_balancer"]["ip"]):
    balancer = cloud.get_balancer(balancer["load_balancer"]["id"])
    print("Waiting Balancer...")
    print(balancer)

# update DNS entry
print("Updating DNS...")
cloud.update_dns(settings.DNS_USERNAME, settings.DNS_PASSWORD, settings.DNS_HOSTNAME, balancer["load_balancer"]["ip"])

# create rke template
print("Criating RKE template...")
rke_template = loader.get_template(
    settings.RKE_TEMPLATE, nodes=nodes, priv_key_file=settings.SSH_PRIV_KEY)
rke_file = open(settings.RKE_DEPLOY_TEMPLATE, "w+")
rke_file.write(rke_template)
rke_file.close()
