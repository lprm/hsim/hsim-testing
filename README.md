![alt text](resources/hsim_testing_logo_132x61.png "HSim Logo")

# HSim Testing

To demonstrate the operation of `HSim` and validate its architectural design, a proof of concept was developed, consisting of two test cases.

The first validates the access to a biomedical data repository and sends it to a health application using one of the communication protocols available in `HSim`. 

The second is a platform scalability test designed to check the behavior of the simulator when the number of patients increases significantly, i.e., if `HSim` supports the increase in the volume of data to be sent to the application. The assessment process will be presented in the following section.

This repository contains all files and instructions necessary to reproduce the testing experiments within `HSim`.

## Step 1 - Set up the HSim simulation cluster

The first step to perform the simulation tests is to configure the Kubernetes cluster. In our tests, we used Rancher (https://rancher.com/) to manage the cluster. We need a cloud service provider, and in this experiment, Digital Ocean service was chosen. To execute this step, it is needed:

* A Digital Ocean account
* A No-Ip account

The configuration step is split into three sub-steps:

* Configure SSH keys for access to the cluster;
* Instantiate the nodes that will be part of the cluster. In this experiment, we are using three nodes.
* Create a load balancer, which will be responsible for meeting requests.
* Configure a DNS entry pointing to the load balancer created in the previous step. Then it is created.
* Use the Rancher Kubernetes Engine (`RKE`) to install a Kubernetes distribution that runs entirely within Docker containers

The `HSim` team provides a docker image with all the tools necessary to perform the cluster configuration to facilitate the entire configuration process.

### Quick Start

The quickest way to get started is using Docker:

```bash
docker run --name hsim-testing -d \
    --env 'CLOUD_PROVIDER_AUTH_KEY=digitalocean_key' \
    --env 'DNS_USERNAME=username' \
    --env 'DNS_PASSWORD=password' \
    --env 'DNS_HOSTNAME=hostname' \
    registry.gitlab.com/lprm/hsim/hsim-testing:0.0.1
```

## Step 2 - Create the project HSim and configure it

In this step, we will create the HSim project using the Rancher interface. Go to the address configured in the previous step.

In the main interface, go to `Projects/Namespaces` menu and click on the buttton `Create project`. In the project name field, put `HSim`, and click on `Create` button.

In the project `HSim`, add the `testing` namespace. To do it, click on `Add namespace` button in the project `HSim`.

## Step 3. Adjust cluster permissions for EMQX

On the `HSim` cluster, click on button `Launch kubectl`. Create a `permissions.yaml` file and paste the following content into it:

```yaml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: fabric8-rbac
subjects:
  - kind: ServiceAccount
    # Reference to upper's `metadata.name`
    name: default
    # Reference to upper's `metadata.namespace`
    namespace: hsim
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
```

To apply the permissions, use the following command:

```bash
kubectl apply -f permissions.yaml
```

## Step 4. Import the configuration files

In the folder `cluster`, `HSim` provides four configuration files. Use the `Import YAML` button, in the `HSim` project, to import each of the four files. Select the option `Namespace: Import all resources into a specific namespace` and the namespace `testing` to proceed with the import.

After importing all the configuration files, it is now possible to perform the tests with `HSim`.

