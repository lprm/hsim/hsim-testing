import os

CONFIG_DIR = os.path.abspath("./config/")

SSH_PRIV_KEY = os.path.join(CONFIG_DIR, "id_rsa")
SSH_PUB_KEY = os.path.join(CONFIG_DIR, "id_rsa.pub")
SSH_KEY_NAME = "testing"

CLOUD_PROVIDER = {
    "PROVIDER": "digitalocean",
    "SIZE": "s-2vcpu-4gb",
    "LOCATION": "nyc1",
    "IMAGE": "18.04.3 (LTS) x64",
    "AUTH": {
        "KEY": os.getenv("CLOUD_PROVIDER_AUTH_KEY", "key"),
        "API_VERSION": "v2"
    },
    "EXTRA_OPTIONS": {'backups': False, 'private_networking': True, 'ssh_keys': []},
}

DNS_USERNAME = os.getenv("DNS_USERNAME", "username")
DNS_PASSWORD = os.getenv("DNS_PASSWORD", "password")
DNS_HOSTNAME = os.getenv("DNS_HOSTNAME", "localhost")

DOCKER_INSTALL_SCRIPT = '''#!/usr/bin/env bash
apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt-get -y update && apt-get -y install docker-ce
'''

RKE_TEMPLATE = "rancher-cluster.yml"
RKE_DEPLOY_TEMPLATE = os.path.join(CONFIG_DIR, RKE_TEMPLATE)