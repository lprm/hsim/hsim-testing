from jinja2 import Environment, FileSystemLoader


class TemplateLoader():

    def __init__(self):
        self.env = Environment(
            loader=FileSystemLoader('./templates')
        )

    def get_template(self, name, **kwags):
        template = self.env.get_template(name)
        return template.render(**kwags)

loader = TemplateLoader()